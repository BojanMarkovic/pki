import Vue from 'vue'
import VueRouter from 'vue-router'
import LoginComponent from "../components/LoginComponent";
import KupacHome from "../components/KupacHome";
import ProdavacHome from "../components/ProdavacHome";
import MojiPodaciKupac from "../components/MojiPodaciKupac";
import IzmenaLozinkeKupac from "../components/IzmenaLozinkeKupac";
import IzmenaPodatakaKupac from "../components/IzmenaPodatakaKupac";
import Proizvodi from "../components/Proizvodi";
import ProizvodInfo from "../components/ProizvodInfo";
import MojaKorpa from "../components/MojaKorpa";
import PorudzbineKupac from "../components/PorudzbineKupac";
import PorudzbinaInfo from "../components/PorudzbinaInfo";
import MojiPodaciProdavac from "../components/MojiPodaciProdavac";
import IzmenaLozinkeProdavac from "../components/IzmenaLozinkeProdavac";
import IzmenaPodatakaProdavac from "../components/IzmenaPodatakaProdavac";
import PorudzbineProdavac from "../components/PorudzbineProdavac";
import PorudzbinaInfoProdavac from "../components/PorudzbinaInfoProdavac";
import ProizvodInfoProdavac from "../components/ProizvodInfoProdavac";
import DodavanjeProizvoda from "../components/DodavanjeProizvoda";


Vue.use(VueRouter)

const routes = [
    {
        path: '/',
        name: 'welcome',
        component: LoginComponent
    },
    {
        path: '/kupacHome',
        name: 'kupacHome',
        component: KupacHome
    },
    {
        path: '/MojiPodaciProdavac',
        name: 'MojiPodaciProdavac',
        component: MojiPodaciProdavac
    },
    {
        path: '/IzmenaLozinkeProdavac',
        name: 'IzmenaLozinkeProdavac',
        component: IzmenaLozinkeProdavac
    },
    {
        path: '/IzmenaPodatakaProdavac',
        name: 'IzmenaPodatakaProdavac',
        component: IzmenaPodatakaProdavac
    },
    {
        path: '/PorudzbineProdavac',
        name: 'PorudzbineProdavac',
        component: PorudzbineProdavac
    },
    {
        path: '/PorudzbinaInfoProdavac/:id',
        name: 'PorudzbinaInfoProdavac',
        component: PorudzbinaInfoProdavac
    },
    {
        path: '/ProdavacHome',
        name: 'ProdavacHome',
        component: ProdavacHome
    },
    {
        path: '/MojiPodaciKupac',
        name: 'MojiPodaciKupac',
        component: MojiPodaciKupac
    },
    {
        path: '/IzmenaLozinkeKupac',
        name: 'IzmenaLozinkeKupac',
        component: IzmenaLozinkeKupac
    },
    {
        path: '/IzmenaPodatakaKupac',
        name: 'IzmenaPodatakaKupac',
        component: IzmenaPodatakaKupac
    },
    {
        path: '/Proizvodi',
        name: 'Proizvodi',
        component: Proizvodi
    },
    {
        path: '/ProizvodInfo/:id',
        name: 'ProizvodInfo',
        component: ProizvodInfo
    },
    {
        path: '/ProizvodInfoProdavac/:id',
        name: 'ProizvodInfoProdavac',
        component: ProizvodInfoProdavac
    },
    {
        path: '/MojaKorpa',
        name: 'MojaKorpa',
        component: MojaKorpa
    },
    {
        path: '/PorudzbineKupac',
        name: 'PorudzbineKupac',
        component: PorudzbineKupac
    },
    {
        path: '/PorudzbinaInfo/:id',
        name: 'PorudzbinaInfo',
        component: PorudzbinaInfo
    },
    {
        path: '/DodavanjeProizvoda',
        name: 'DodavanjeProizvoda',
        component: DodavanjeProizvoda
    }
]

const router = new VueRouter({
    mode: 'history',
    base: process.env.BASE_URL,
    routes
})

export default router
