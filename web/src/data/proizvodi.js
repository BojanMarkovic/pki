const proizvodi = [
    {
        id: 0,
        name: "Forever Royal jelly",
        price: "500din",
        picture: "jelly.jpg",
        nacinKoriscenja: "Ovo je način korišćenja ovog proizvoda",
        opis: "Ovo je opis proizvoda Forever royal jelly. Proizvod je od meda.",
        kolicina: 0
    },
    {
        id: 1,
        name: "Billy bee med",
        price: "400din",
        picture: "billy.jpg",
        nacinKoriscenja: "Ovo je način korišćenja ovog proizvoda",
        opis: "Ovo je opis proizvoda Billy bee med. Proizvod je od meda.",
        kolicina: 0
    },
    {
        id: 2,
        name: "Pčelinji polen",
        price: "500din",
        picture: "polen.jpg",
        nacinKoriscenja: "Ovo je način korišćenja ovog proizvoda",
        opis: "Ovo je opis proizvoda Pčelinji polen. Proizvod je od meda.",
        kolicina: 0
    },
    {
        id: 3,
        name: "Sveće od meda",
        price: "200din",
        picture: "svece.jpg",
        nacinKoriscenja: "Ovo je način korišćenja ovog proizvoda",
        opis: "Ovo je opis proizvoda Sveće od meda. Proizvod je od meda.",
        kolicina: 0
    },
    {
        id: 4,
        name: "Livadski med",
        price: "800din",
        picture: "livadski.jpg",
        nacinKoriscenja: "Ovo je način korišćenja ovog proizvoda",
        opis: "Ovo je opis proizvoda Livadski med. Proizvod je od meda.",
        kolicina: 0
    }
]

export default proizvodi;