const users = [
    {
        username: 'pera',
        password: '123',
        ime: 'Pera',
        prezime: 'Peric',
        kontaktTelefon: '44545546',
        adresa: 'Nemanjina',
        tipKorisnika: "0",//kupac=0 prodavac=1
        proizvodiUKorpi: [
            {
                id: 0,
                kolicina: 2
            },
            {
                id: 1,
                kolicina: 3
            }],
        porudzbine: [
            {
                id: 0,
                datum: '12.12.2020.',
                ukupnaCena: '2000 din',
                status: 'Odbijeno',
                proizvodi: [
                    {
                        id: 0,
                        name: "Forever Royal jelly",
                        price: "500din",
                        picture: "jelly.jpg",
                        nacinKoriscenja: "Ovo je način korišćenja ovog proizvoda",
                        opis: "Ovo je opis proizvoda Forever royal jelly. Proizvod je od meda.",
                        kolicina: 4
                    }
                ]
            },
            {
                id: 1,
                datum: '02.12.2020.',
                ukupnaCena: '2300 din',
                status: 'Odbijeno',
                proizvodi: [
                    {
                        id: 2,
                        name: "Pčelinji polen",
                        price: "500din",
                        picture: "polen.jpg",
                        nacinKoriscenja: "Ovo je način korišćenja ovog proizvoda",
                        opis: "Ovo je opis proizvoda Pčelinji polen. Proizvod je od meda.",
                        kolicina: 3
                    },
                    {
                        id: 4,
                        name: "Livadski med",
                        price: "800din",
                        picture: "livadski.jpg",
                        nacinKoriscenja: "Ovo je način korišćenja ovog proizvoda",
                        opis: "Ovo je opis proizvoda Livadski med. Proizvod je od meda.",
                        kolicina: 1
                    }
                ]
            }
        ]
    },
    {
        username: 'zika',
        password: '123',
        ime: 'Zika',
        prezime: 'Zikic',
        kontaktTelefon: '2123245',
        adresa: 'Bulevar A',
        tipKorisnika: "1",//kupac=0 prodavac=1
        proizvodiUKorpi: [
            {
                id: 1,
                kolicina: 5
            },
            {
                id: 3,
                kolicina: 3
            }],
        porudzbine: []
    },
    {
        username: 'admin',
        password: '123',
        ime: 'Admin',
        prezime: 'Adminic',
        kontaktTelefon: '123123123',
        adresa: 'Bulevar B',
        tipKorisnika: "0",//kupac=0 prodavac=1
        proizvodiUKorpi: [
            {
                id: 2,
                kolicina: 4
            },
            {
                id: 3,
                kolicina: 1
            }],
        porudzbine: [
            {
                id: 0,
                datum: '12.12.2020.',
                ukupnaCena: '2000 din',
                status: 'Odbijeno',
                proizvodi: [
                    {
                        id: 0,
                        name: "Forever Royal jelly",
                        price: "500din",
                        picture: "jelly.jpg",
                        nacinKoriscenja: "Ovo je način korišćenja ovog proizvoda",
                        opis: "Ovo je opis proizvoda Forever royal jelly. Proizvod je od meda.",
                        kolicina: 4
                    }
                ]
            }
        ]
    },
]

export default users;