package com.example.mb203014m

import android.content.Intent
import android.os.Bundle
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.google.android.material.textfield.TextInputEditText


class LoginActivity : AppCompatActivity() {
    class Element(
        var name: String, var surname: String, var password: String, var username: String,
        var address: String, var phoneNumber: String, val tip: Int
    ) //tip=0 kupac

    companion object {
        val userInfo = hashMapOf(
            "admin" to Element(
                "admin", "prezime", "admin",
                "admin", "adresa", "99999999", 0
            ), "pera9" to Element(
                "pera", "prezime",
                "pera9", "pera9", "adresa2", "333333333", 0
            )
        )
        lateinit var currentUser: Element
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.login_page)

        val loginButton = findViewById<Button>(R.id.login_button)
        loginButton.setOnClickListener {
            val username = findViewById<TextView>(R.id.korisnicko_ime_login_input).text.toString()
            val password = findViewById<TextView>(R.id.lozinka_login_input).text.toString()
            if (userInfo.containsKey(username)) {
                if (userInfo[username]?.password?.equals(password)!!) {
                    currentUser = userInfo[username]!!
                    startActivity(Intent(this, MainActivity::class.java))
                } else {
                    findViewById<TextInputEditText>(R.id.lozinka_login_input).error =
                        "Šifra nije validna!"
                    Toast.makeText(
                        it.context,
                        "Postoje greške u unetim podacima!",
                        Toast.LENGTH_LONG
                    ).show()
                }
            } else {
                findViewById<TextInputEditText>(R.id.korisnicko_ime_login_input).error =
                    "Korisničko ime ne postoji!"
                Toast.makeText(
                    it.context,
                    "Postoje greške u unetim podacima!",
                    Toast.LENGTH_LONG
                ).show()
            }
        }

        val registerText = findViewById<TextView>(R.id.register_text)
        registerText.setOnClickListener {
            startActivity(Intent(this, RegisterActivity::class.java))
        }
    }
}