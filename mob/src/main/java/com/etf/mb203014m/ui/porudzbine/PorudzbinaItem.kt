package com.example.mb203014m.ui.porudzbine

import android.app.Activity
import android.content.Context
import android.widget.LinearLayout
import android.widget.TextView
import com.example.mb203014m.R

class PorudzbinaItem(context: Context?) : LinearLayout(context) {
    var textCenaPorudzbine: TextView? = null
    var textDatumPorudzbine: TextView? = null
    var textStatusPorudzbine: TextView? = null
    var id: TextView? = null

    init {
        (getContext() as Activity).layoutInflater.inflate(R.layout.cards_porudzbine, this)
        textCenaPorudzbine = findViewById(R.id.cena_porudzbine_cards)
        textDatumPorudzbine = findViewById(R.id.datum_porudzbine_cards)
        textStatusPorudzbine = findViewById(R.id.status_porudzbina_cards)
        id = findViewById(R.id.id_hidden)
    }
}