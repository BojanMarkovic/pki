package com.example.mb203014m.ui.proizvodi

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import androidx.fragment.app.Fragment
import com.example.mb203014m.R


class ProizvodiFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val root = inflater.inflate(R.layout.fragment_proizvodi, container, false)
        val linearLayout = root.findViewById<LinearLayout>(R.id.layout_porizvodi_main)

        for ((_, item) in proizvodiInfo) {
            val proizvod = Proizvod(context)
            proizvod.textContent?.text = item.name
            proizvod.textPrice?.text = item.price
            proizvod.imageContent?.setImageResource(item.image)
            proizvod.setOnClickListener {
                val intent = Intent(activity, DetaljiProizvod::class.java)
                intent.putExtra(
                    "com.example.mb203014m.ui.id",
                    it.findViewById<TextView>(R.id.cards_name).text.toString()
                )
                startActivity(intent)
            }
            linearLayout.addView(proizvod)
        }

        return root
    }

    class Element(
        val name: String,
        val price: String,
        val image: Int,
        val koriscenje: String,
        val opis: String
    )

    companion object {
        val proizvodiInfo = hashMapOf(
            "Forever Royal jelly" to Element(
                "Forever Royal jelly",
                "500din",
                R.drawable.jelly,
                "Ovo je način korišćenja ovog proizvoda",
                "Ovo je opis proizvoda Forever royal jelly. Proizvod je od meda."
            ), "Billy bee med" to Element(
                "Billy bee med",
                "400din",
                R.drawable.billy,
                "Ovo je način korišćenja ovog proizvoda",
                "Ovo je opis proizvoda Billy bee med. Proizvod je od meda."
            ), "Pčelinji polen" to Element(
                "Pčelinji polen",
                "500din",
                R.drawable.polen,
                "Ovo je način korišćenja ovog proizvoda",
                "Ovo je opis proizvoda Pčelinji polen. Proizvod je od meda."
            ), "Sveće od meda" to Element(
                "Sveće od meda", "200din", R.drawable.svece,
                "Ovo je način korišćenja ovog proizvoda",
                "Ovo je opis proizvoda Sveće od meda. Proizvod je od meda."
            ), "Livadski med" to Element(
                "Livadski med",
                "800din", R.drawable.livadski, "Ovo je način korišćenja ovog proizvoda",
                "Ovo je opis proizvoda Livadski med. Proizvod je od meda."
            )
        )
    }
}