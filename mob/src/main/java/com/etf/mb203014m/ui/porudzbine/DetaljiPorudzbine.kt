package com.example.mb203014m.ui.porudzbine

import android.content.Intent
import android.os.Bundle
import android.widget.LinearLayout
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import com.example.mb203014m.R
import com.example.mb203014m.ui.mojaKorpa.KorpaItem
import com.example.mb203014m.ui.proizvodi.DetaljiProizvod
import com.example.mb203014m.ui.proizvodi.ProizvodiFragment

class DetaljiPorudzbine : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.porudzbina_detalji)

        val id = intent.getStringExtra("com.example.mb203014m.ui.id")
        val linearLayout = findViewById<LinearLayout>(R.id.layout_porudzbina_detalji)

        val toolbar: Toolbar = findViewById(R.id.toolbar5)
        toolbar.title = "Detalji porudžbine"
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)
        toolbar.setNavigationOnClickListener {
            navigateUpTo(Intent(this, PorudzbineFragment::class.java))
        }

        findViewById<TextView>(R.id.datum_porudzbine_cards_detalji).text=PorudzbineFragment.porudzbineInfo[id]?.datum
        findViewById<TextView>(R.id.cena_porudzbine_cards_detalji).text=PorudzbineFragment.porudzbineInfo[id]?.price
        findViewById<TextView>(R.id.status_porudzbina_cards_detalji).text=PorudzbineFragment.porudzbineInfo[id]?.status

        PorudzbineFragment.porudzbineInfo[id]?.proizvodi?.forEach { (name, kolicina) ->
            val item = ProizvodiFragment.proizvodiInfo[name]
            val artikal = KorpaItem(this, null, false)
            artikal.textCenaKomad?.text = item?.price
            artikal.textKolicina?.text = kolicina
            artikal.textNaslov?.text = name
            artikal.textUkupnaCena?.text = ((item?.price?.replace("din", "")?.toInt()
                ?: 0) * kolicina.toInt()).toString() + "din"
            artikal.setOnClickListener {
                val intent = Intent(this, DetaljiProizvod::class.java)
                it.findViewById<TextView>(R.id.naslov_korpa_pojedinacno).text.toString()

                intent.putExtra(
                    "com.example.mb203014m.ui.id",
                    it.findViewById<TextView>(R.id.naslov_korpa_pojedinacno).text.toString()
                )
                startActivity(intent)
            }
            linearLayout.addView(artikal)
        }
    }
}