package com.example.mb203014m.ui.mojaKorpa


import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.LinearLayout
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.Fragment
import com.example.mb203014m.R
import com.example.mb203014m.ui.porudzbine.PorudzbineFragment
import com.example.mb203014m.ui.proizvodi.DetaljiProizvod
import com.example.mb203014m.ui.proizvodi.ProizvodiFragment
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter
import java.util.function.Consumer

class MojaKorpaFragment : Fragment() {
    override fun onResume() {
        super.onResume()
        resumeFlag = if (!resumeFlag) {
            refresh()
            true
        } else {
            false
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val root = inflater.inflate(R.layout.fragment_moja_korpa, container, false)
        val linearLayout = root.findViewById<LinearLayout>(R.id.layout_korpa_main)
        val naruci = root.findViewById<Button>(R.id.naruci_button)
        naruci.setOnClickListener {
            AlertDialog.Builder(it.context).setIcon(android.R.drawable.ic_dialog_alert)
                .setTitle("Naručivanje!")
                .setMessage("Da li ste sigurni da želite da naručite ove proizvode?")
                .setPositiveButton("Da") { _, _ ->

                    var max = 0
                    PorudzbineFragment.porudzbineInfo.keys.forEach(Consumer { id ->
                        if (id.toInt() > max) max = id.toInt()
                    })
                    max++
                    val date = LocalDateTime.now().format(DateTimeFormatter.ofPattern("dd.MM.yyyy"))
                    val proizvodi = hashMapOf<String, String>()
                    korpaInfo.forEach { (name, info) ->
                        proizvodi[name] = info.kolicina
                    }
                    PorudzbineFragment.porudzbineInfo[max.toString()] = PorudzbineFragment.Element(
                        max.toString(), date,
                        (it.parent as ViewGroup).findViewById<TextView>(R.id.ukupna_cena_korpa_vrednost).text.toString(),
                        "U obradi", proizvodi
                    )
                    korpaInfo.clear()
                    refresh()
                }.setNegativeButton("Ne", null).show()
        }

        var ukupno: Int? = 0
        for ((_, item) in korpaInfo) {
            val artikal = KorpaItem(context, this, true)
            artikal.textCenaKomad?.text = ProizvodiFragment.proizvodiInfo[item.name]?.price
            artikal.textKolicina?.text = item.kolicina
            artikal.textNaslov?.text = item.name
            artikal.textUkupnaCena?.text = item.ukupno
            ukupno = ukupno?.plus(item.ukupno.replace("din", "").toInt())
            artikal.setOnClickListener {
                val intent = Intent(activity, DetaljiProizvod::class.java)
                intent.putExtra(
                    "com.example.mb203014m.ui.id",
                    it.findViewById<TextView>(R.id.naslov_korpa_pojedinacno).text.toString()
                )
                startActivity(intent)
            }
            linearLayout.addView(artikal)
        }
        root.findViewById<TextView>(R.id.ukupna_cena_korpa_vrednost).text =
            ukupno.toString() + "din"
        return root
    }

    fun refresh() {
        this.parentFragmentManager.beginTransaction().detach(this).attach(this).commit()
    }

    class Element(
        val name: String,
        var kolicina: String,
        var ukupno: String
    )

    companion object {
        var resumeFlag: Boolean = false
        var korpaInfo =
            hashMapOf(
                "Forever Royal jelly" to Element("Forever Royal jelly", "2", "1000din"),
                "Billy bee med" to Element("Billy bee med", "4", "1600din")
            )
    }
}