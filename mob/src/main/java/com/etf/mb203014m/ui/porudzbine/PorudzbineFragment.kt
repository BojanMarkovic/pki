package com.example.mb203014m.ui.porudzbine

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import androidx.fragment.app.Fragment
import com.example.mb203014m.R

class PorudzbineFragment : Fragment() {


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val root = inflater.inflate(R.layout.fragment_porudzbine, container, false)
        val linearLayout = root.findViewById<LinearLayout>(R.id.layout_porudzbine_main)


        for ((_, item) in porudzbineInfo) {
            val porudzbina = PorudzbinaItem(context)
            porudzbina.textCenaPorudzbine?.text = item.price
            porudzbina.textDatumPorudzbine?.text = item.datum
            porudzbina.textStatusPorudzbine?.text = item.status
            porudzbina.id?.text = item.id
            porudzbina.setOnClickListener {
                val intent = Intent(activity, DetaljiPorudzbine::class.java)
                intent.putExtra(
                    "com.example.mb203014m.ui.id",
                    it.findViewById<TextView>(R.id.id_hidden).text.toString()
                )
                startActivity(intent)
            }
            linearLayout.addView(porudzbina)
        }

        return root
    }

    class Element(
        val id: String,
        val datum: String,
        val price: String,
        val status: String,
        val proizvodi: HashMap<String, String>
    )

    companion object {
        var porudzbineInfo = hashMapOf(
            "0" to Element(
                "0", "24.11.2020.", "2500din", "Prihvaćeno, dostava 4 dana", hashMapOf(
                    "Billy bee med" to "5", "Pčelinji polen" to "1"
                )
            ),
            "1" to Element(
                "1", "12.12.2020.", "1600din", "Odbijeno", hashMapOf(
                    "Livadski med" to "1", "Sveće od meda" to "4"
                )
            )
        )
    }
}