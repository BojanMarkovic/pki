package com.example.mb203014m.ui.proizvodi

import android.app.Activity
import android.content.Context
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import com.example.mb203014m.R

class Proizvod(context: Context?) : LinearLayout(context) {

    var textContent: TextView? = null
    var textPrice: TextView? = null
    var imageContent: ImageView? = null

    init {
        (getContext() as Activity).layoutInflater.inflate(R.layout.cards_layout, this)
        textContent = findViewById(R.id.cards_name)
        textPrice = findViewById(R.id.cards_price)
        imageContent = findViewById(R.id.imageView_cards)
    }
}