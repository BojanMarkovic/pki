package com.example.mb203014m.ui.dobrodosli

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.mb203014m.LoginActivity

class DobrodosliViewModel : ViewModel() {

    private val _text = MutableLiveData<String>().apply {
        value = "Kupac: " + LoginActivity.currentUser.name + "\nDobrodošli u aplikaciju."
    }
    val text: LiveData<String> = _text
}