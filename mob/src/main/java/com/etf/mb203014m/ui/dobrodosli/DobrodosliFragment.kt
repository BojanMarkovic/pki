package com.example.mb203014m.ui.dobrodosli

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.example.mb203014m.R

class DobrodosliFragment : Fragment() {

    private lateinit var dobrodosliViewModel: DobrodosliViewModel

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        dobrodosliViewModel =
            ViewModelProvider(this).get(DobrodosliViewModel::class.java)
        val root = inflater.inflate(R.layout.fragment_dobrodosli, container, false)
        val textView: TextView = root.findViewById(R.id.text_dobrodosli)
        dobrodosliViewModel.text.observe(viewLifecycleOwner, Observer {
            textView.text = it
        })
        return root
    }
}