package com.example.mb203014m.ui.proizvodi

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import com.example.mb203014m.R
import com.example.mb203014m.ui.mojaKorpa.MojaKorpaFragment
import com.google.android.material.textfield.TextInputEditText

class DetaljiProizvod : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.proizvod_detalji)

        val id = intent.getStringExtra("com.example.mb203014m.ui.id")

        val toolbar: Toolbar = findViewById(R.id.toolbar4)
        toolbar.title = "Detalji proizvoda"
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)
        toolbar.setNavigationOnClickListener {
            navigateUpTo(Intent(this, ProizvodiFragment::class.java))
        }

        val dodajUKorpu = findViewById<Button>(R.id.dodaj_u_korpu)
        dodajUKorpu.setOnClickListener {
            val kolicina = (((it.parent as View) as ViewGroup).getChildAt(11)as TextView).text.toString()
            val cena = (((it.parent as View) as ViewGroup).getChildAt(5)as TextView).text.toString()
            val name = (((it.parent as View) as ViewGroup).getChildAt(2)as TextView).text.toString()

            AlertDialog.Builder(it.context).setIcon(android.R.drawable.ic_dialog_alert)
                .setTitle("Dodavanje u korpu!")
                .setMessage("Da li ste sigurni da želite da stavite $name u vašu korpu, količina: $kolicina?")
                .setPositiveButton("Da") { _, _ ->
                    if (MojaKorpaFragment.korpaInfo.containsKey(name)) {
                        val staraKolicina = MojaKorpaFragment.korpaInfo[name]?.kolicina?.toInt()
                        MojaKorpaFragment.korpaInfo[name] = MojaKorpaFragment.Element(
                            name, (kolicina.toInt() + staraKolicina!!).toString(),
                            (cena.replace("din", "")
                                .toInt() * (kolicina.toInt() + staraKolicina)).toString() + "din"
                        )
                    } else {
                        MojaKorpaFragment.korpaInfo[name] = MojaKorpaFragment.Element(
                            name, kolicina,
                            (cena.replace("din", "").toInt() * kolicina.toInt()).toString() + "din"
                        )
                    }
                    navigateUpTo(Intent(this, ProizvodiFragment::class.java))

                    Toast.makeText(
                        it.context,
                        "Proizvod je uspešno dodat u korpu!",
                        Toast.LENGTH_LONG
                    )
                        .show()
                }.setNegativeButton("Ne", null).show()
        }

        findViewById<TextView>(R.id.detalji_proizvoda_opis).text =
            ProizvodiFragment.proizvodiInfo[id]?.opis
        findViewById<TextView>(R.id.detalji_proizvoda_opis2).text =
            ProizvodiFragment.proizvodiInfo[id]?.koriscenje
        findViewById<TextView>(R.id.proizvod_detalji_cena).text =
            ProizvodiFragment.proizvodiInfo[id]?.price
        findViewById<EditText>(R.id.kolicina_input_korpa).setText("1")
        findViewById<TextView>(R.id.proizvod_detalji_naslov_main).text =
            ProizvodiFragment.proizvodiInfo[id]?.name
        ProizvodiFragment.proizvodiInfo[id]?.let {
            findViewById<ImageView>(R.id.imageView).setImageResource(
                it.image
            )
        }
    }
}