package com.example.mb203014m.ui.mojiPodaci

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.mb203014m.LoginActivity
import com.example.mb203014m.RefreshableLiveData

class MojiPodaciViewModel : ViewModel() {

    private val title = RefreshableLiveData<String> {
        MutableLiveData<String>().apply {
            value = "Podaci o kupcu: " + LoginActivity.currentUser.name
        }
    }
    private val name = RefreshableLiveData<String> {
        MutableLiveData<String>().apply {
            value = LoginActivity.currentUser.name
        }
    }
    private val surname = RefreshableLiveData<String> {
        MutableLiveData<String>().apply {
            value = LoginActivity.currentUser.surname
        }
    }
    private val number = RefreshableLiveData<String> {
        MutableLiveData<String>().apply {
            value = LoginActivity.currentUser.phoneNumber
        }
    }
    private val adress = RefreshableLiveData<String> {
        MutableLiveData<String>().apply {
            value = LoginActivity.currentUser.address
        }
    }
    private val username = RefreshableLiveData<String> {
        MutableLiveData<String>().apply {
            value = LoginActivity.currentUser.username
        }
    }


    val titleText: LiveData<String> = title
    val nameText: LiveData<String> = name
    val surnameText: LiveData<String> = surname
    val numberText: LiveData<String> = number
    val adressText: LiveData<String> = adress
    val usernameText: LiveData<String> = username

    fun refreshAll() {
        title.refresh()
        name.refresh()
        surname.refresh()
        number.refresh()
        username.refresh()
        adress.refresh()
    }
}