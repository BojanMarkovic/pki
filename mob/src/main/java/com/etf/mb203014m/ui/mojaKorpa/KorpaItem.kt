package com.example.mb203014m.ui.mojaKorpa

import android.app.Activity
import android.content.Context
import android.content.DialogInterface
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import com.example.mb203014m.R
import com.google.android.material.floatingactionbutton.FloatingActionButton


class KorpaItem(context: Context?, mojaKorpaFragment: MojaKorpaFragment?, visibleDelete: Boolean) :
    LinearLayout(context) {
    var textCenaKomad: TextView? = null
    var textKolicina: TextView? = null
    var textUkupnaCena: TextView? = null
    var textNaslov: TextView? = null

    init {
        (getContext() as Activity).layoutInflater.inflate(R.layout.cards_korpa, this)
        textCenaKomad = findViewById(R.id.cena_komad_korpa)
        textKolicina = findViewById(R.id.kolicina_korpa)
        textUkupnaCena = findViewById(R.id.ukupna_cena_korpa_pojedinacno)
        textNaslov = findViewById(R.id.naslov_korpa_pojedinacno)

        val obrisiButton = findViewById<FloatingActionButton>(R.id.obrisi_button_korpa)
        if (visibleDelete) {
            obrisiButton.setOnClickListener {
                val name = (it.parent as ViewGroup)
                    .findViewById<TextView>(R.id.naslov_korpa_pojedinacno).text.toString()

                AlertDialog.Builder(this.context).setIcon(android.R.drawable.ic_dialog_alert)
                    .setTitle("Brisanje proizvoda!")
                    .setMessage("Da li ste sigurni da želite da obrišete $name iz vaše korpe?")
                    .setPositiveButton("Da") { _, _ ->
                        MojaKorpaFragment.korpaInfo.remove(name)
                        mojaKorpaFragment?.refresh()
                        Toast.makeText(
                            this.context,
                            "Proizvod je uspešno obrisan!",
                            Toast.LENGTH_LONG
                        ).show()
                    }.setNegativeButton("Ne", null).show()
            }
        } else {
            obrisiButton.visibility = View.INVISIBLE
        }
    }
}