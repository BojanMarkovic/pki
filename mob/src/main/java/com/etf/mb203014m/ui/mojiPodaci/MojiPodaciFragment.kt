package com.example.mb203014m.ui.mojiPodaci

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.example.mb203014m.IzmeniLozinkuActivity
import com.example.mb203014m.IzmeniPodatkeActivity
import com.example.mb203014m.R

class MojiPodaciFragment : Fragment() {

    private lateinit var mojiPodaciViewModel: MojiPodaciViewModel

    override fun onResume() {
        super.onResume()
        mojiPodaciViewModel.refreshAll()
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        mojiPodaciViewModel = ViewModelProvider(this).get(MojiPodaciViewModel::class.java)
        val root = inflater.inflate(R.layout.fragment_moji_podaci, container, false)

        val izmenaPodataka: Button = root.findViewById(R.id.izmeni_podatke)
        izmenaPodataka.setOnClickListener {
            startActivity(Intent(activity, IzmeniPodatkeActivity::class.java))
        }

        val izmenaLozinke: Button = root.findViewById(R.id.izmeni_lozinku)
        izmenaLozinke.setOnClickListener {
            startActivity(Intent(activity, IzmeniLozinkuActivity::class.java))
        }

        val textNaslov: TextView = root.findViewById(R.id.moji_podaci_naslov)
        val textIme: TextView = root.findViewById(R.id.moji_podaci_ime)
        val textPrezime: TextView = root.findViewById(R.id.moji_podaci_prezime)
        val textTelefon: TextView = root.findViewById(R.id.moji_podaci_telefon)
        val textAdresa: TextView = root.findViewById(R.id.moji_podaci_adresa)
        val textKorisnickoIme: TextView = root.findViewById(R.id.moji_podaci_korisnicko_ime)
        mojiPodaciViewModel.titleText.observe(viewLifecycleOwner, Observer {
            textNaslov.text = it
        })
        mojiPodaciViewModel.nameText.observe(viewLifecycleOwner, Observer {
            textIme.text = it
        })
        mojiPodaciViewModel.surnameText.observe(viewLifecycleOwner, Observer {
            textPrezime.text = it
        })
        mojiPodaciViewModel.numberText.observe(viewLifecycleOwner, Observer {
            textTelefon.text = it
        })
        mojiPodaciViewModel.adressText.observe(viewLifecycleOwner, Observer {
            textAdresa.text = it
        })
        mojiPodaciViewModel.usernameText.observe(viewLifecycleOwner, Observer {
            textKorisnickoIme.text = it
        })
        return root
    }
}