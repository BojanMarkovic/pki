package com.example.mb203014m.ui.odjaviSe

import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import com.example.mb203014m.LoginActivity

class OdjaviSeFragment : Fragment() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        startActivity(Intent(activity, LoginActivity::class.java))
    }
}