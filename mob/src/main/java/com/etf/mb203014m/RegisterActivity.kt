package com.example.mb203014m

import android.content.Intent
import android.os.Bundle
import android.widget.RadioGroup
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.google.android.material.textfield.TextInputEditText

class RegisterActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.register_page)

        val registerButton = findViewById<TextView>(R.id.register_button)
        registerButton.setOnClickListener {
            val username =
                findViewById<TextView>(R.id.korisnicko_ime_register_input).text.toString()
            val password = findViewById<TextView>(R.id.lozinka_register_input).text.toString()
            val passwordAgain =
                findViewById<TextView>(R.id.potvrda_lozinke_register_input).text.toString()
            val name = findViewById<TextView>(R.id.ime_register_input).text.toString()
            val surname = findViewById<TextView>(R.id.prezime_register_input).text.toString()
            val address = findViewById<TextView>(R.id.adresa_register_input).text.toString()
            val number = findViewById<TextView>(R.id.kontakt_telefon_register_input).text.toString()
            val type =
                resources.getResourceEntryName(findViewById<RadioGroup>(R.id.radioGroup2).checkedRadioButtonId)
            if (LoginActivity.userInfo.containsKey(username)) {
                findViewById<TextInputEditText>(R.id.korisnicko_ime_register_input).error =
                    "Korisničko ime je zauzeto!"
                Toast.makeText(
                    it.context,
                    "Postoje greške u unetim podacima!",
                    Toast.LENGTH_LONG
                ).show()
            } else {
                if (password == passwordAgain) {
                    if (type == "kupac") {
                        LoginActivity.userInfo[username] = LoginActivity.Element(
                            name,
                            surname,
                            password,
                            username,
                            address,
                            number,
                            0
                        )
                        LoginActivity.currentUser = LoginActivity.userInfo[username]!!
                        startActivity(Intent(this, MainActivity::class.java))
                    } else {
                        Toast.makeText(
                            it.context,
                            "Uspešno ste registrovani! Prodavci moraju pristupiti aplikaciji preko pretraživača.",
                            Toast.LENGTH_LONG
                        ).show()
                        startActivity(Intent(this, LoginActivity::class.java))
                    }
                } else {
                    findViewById<TextInputEditText>(R.id.potvrda_lozinke_register_input).error =
                        "Potvrda šifre se ne podudara sa unetom šifrom!"
                    Toast.makeText(
                        it.context,
                        "Postoje greške u unetim podacima!",
                        Toast.LENGTH_LONG
                    ).show()
                }
            }
        }

        val loginText = findViewById<TextView>(R.id.login_text)
        loginText.setOnClickListener {
            startActivity(Intent(this, LoginActivity::class.java))
        }
    }
}