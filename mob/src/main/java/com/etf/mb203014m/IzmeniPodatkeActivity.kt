package com.example.mb203014m

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.widget.Toolbar
import com.example.mb203014m.ui.mojaKorpa.MojaKorpaFragment
import com.example.mb203014m.ui.mojiPodaci.MojiPodaciFragment
import com.google.android.material.textfield.TextInputEditText
import com.google.android.material.textfield.TextInputLayout
import java.util.function.BiConsumer

class IzmeniPodatkeActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_izmeni_podatke)

        val toolbar: Toolbar = findViewById(R.id.toolbar2)
        toolbar.title = "Izmena podataka"
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)
        toolbar.setNavigationOnClickListener {
            navigateUpTo(Intent(this, MojiPodaciFragment::class.java))
        }

        findViewById<TextView>(R.id.izmena_podataka_korisnicko_ime).text =
            LoginActivity.currentUser.username
        findViewById<TextView>(R.id.izmena_podataka_ime).text = LoginActivity.currentUser.name
        findViewById<TextView>(R.id.izmena_podataka_prezime).text =
            LoginActivity.currentUser.surname
        findViewById<TextView>(R.id.izmena_podataka_adresa).text = LoginActivity.currentUser.address
        findViewById<TextView>(R.id.izmena_podataka_telefon).text =
            LoginActivity.currentUser.phoneNumber

        val odustani = findViewById<Button>(R.id.primeni_izmene_odustani)
        odustani.setOnClickListener {
            navigateUpTo(Intent(this, MojiPodaciFragment::class.java))
        }

        val primeniIzmene = findViewById<Button>(R.id.primeni_izmene_podataka)
        primeniIzmene.setOnClickListener {
            val username =
                findViewById<TextView>(R.id.izmena_podataka_korisnicko_ime).text.toString()
            if (!unique(username)) {
                findViewById<TextInputEditText>(R.id.izmena_podataka_korisnicko_ime).error =
                    "Korisničko ime je već u upotrebi!"
                Toast.makeText(
                    it.context,
                    "Postoje greške u unetim podacima!",
                    Toast.LENGTH_LONG
                ).show()
            } else {

                LoginActivity.userInfo.remove(LoginActivity.currentUser.username)
                val name = findViewById<TextView>(R.id.izmena_podataka_ime).text.toString()
                val surname =
                    findViewById<TextView>(R.id.izmena_podataka_prezime).text.toString()
                val address =
                    findViewById<TextView>(R.id.izmena_podataka_adresa).text.toString()
                val number =
                    findViewById<TextView>(R.id.izmena_podataka_telefon).text.toString()
                LoginActivity.currentUser.address = address
                LoginActivity.currentUser.surname = surname
                LoginActivity.currentUser.phoneNumber = number
                LoginActivity.currentUser.name = name
                LoginActivity.currentUser.username = username
                LoginActivity.userInfo[username] = LoginActivity.currentUser
                navigateUpTo(Intent(this, MojiPodaciFragment::class.java))

                Toast.makeText(
                    it.context,
                    "Uspešno promenjene informacije!",
                    Toast.LENGTH_LONG
                ).show()
            }
        }
    }

    private fun unique(username: String): Boolean {
        LoginActivity.userInfo.forEach { (name, _) ->
            if ((name == username) && (name != LoginActivity.currentUser.username)) {
                return false
            }
        }
        return true
    }
}