package com.example.mb203014m

import android.content.Intent
import android.os.Bundle
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import com.example.mb203014m.ui.mojaKorpa.MojaKorpaFragment
import com.example.mb203014m.ui.mojiPodaci.MojiPodaciFragment
import com.google.android.material.textfield.TextInputEditText

class IzmeniLozinkuActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_izmeni_lozinku)

        val toolbar: Toolbar = findViewById(R.id.toolbar3)
        toolbar.title = "Izmena lozinke"
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)
        toolbar.setNavigationOnClickListener {
            navigateUpTo(Intent(this, MojiPodaciFragment::class.java))
        }

        val odustani = findViewById<Button>(R.id.izmena_lozinke_odustani)
        odustani.setOnClickListener {
            navigateUpTo(Intent(this, MojiPodaciFragment::class.java))
        }

        val primeniIzmene = findViewById<Button>(R.id.primeni_izmenu_lozinke)
        primeniIzmene.setOnClickListener {
            val oldPassword =
                findViewById<TextView>(R.id.izmena_lozinke_stara).text.toString()
            val newPasword =
                findViewById<TextView>(R.id.izmena_lozinke_nova).text.toString()
            val new2 = findViewById<TextView>(R.id.izmena_lozinke_potvrda).text.toString()
            if (oldPassword == LoginActivity.currentUser.password) {
                if (newPasword == new2) {
                    LoginActivity.currentUser.password = newPasword
                    LoginActivity.userInfo[LoginActivity.currentUser.username] =
                        LoginActivity.currentUser
                    navigateUpTo(Intent(this, MojiPodaciFragment::class.java))
                    Toast.makeText(
                        it.context,
                        "Uspešno promenjena lozinka!",
                        Toast.LENGTH_LONG
                    ).show()
                } else {
                    findViewById<TextInputEditText>(R.id.izmena_lozinke_potvrda).error =
                        "Potvrda šifre se ne podudara sa unetom šifrom!"
                    Toast.makeText(
                        it.context,
                        "Postoje greške u unetim podacima!",
                        Toast.LENGTH_LONG
                    ).show()
                }
            } else {
                findViewById<TextInputEditText>(R.id.izmena_lozinke_stara).error =
                    "Šifra nije validna!"
                Toast.makeText(
                    it.context,
                    "Postoje greške u unetim podacima!",
                    Toast.LENGTH_LONG
                ).show()
            }
        }
    }
}